import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static List<Integer> listSplit = new ArrayList<Integer>();

    public static void main(String[] args){

        try {
            writeFile();
            readFile();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Collections.sort(listSplit);
        System.out.println("Сортировка по возрастанию:");
        for (int item : listSplit) {
            System.out.println(item);
        }

        Collections.reverse(listSplit);
        System.out.println("Сортировка по убыванию:");
        for (int item : listSplit) {
            System.out.println(item);
        }
    }

    public static void writeFile() throws IOException {
        int min = 0;
        int max = 20;
        String wrileLine="";

        FileOutputStream fileOutputStream = new FileOutputStream("C:\\Users\\о\\IdeaProjects\\TestTaskAlfa_2\\files\\file.txt");
        for(int i=0;i<max;i++){
            wrileLine +=  min + (int) (Math.random() * max )+ ",";
        }
        fileOutputStream.write(wrileLine.getBytes());
        fileOutputStream.close();
    }

    public static void readFile() throws IOException {
        String readLine="";

        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\о\\IdeaProjects\\TestTaskAlfa_2\\files\\file.txt");

        int i;
        while((i=fileInputStream.read())!= -1){

            readLine += (char)i;
        }

        for(int j=0; j<readLine.split("\\,").length;j++ ) {
            listSplit.add(Integer.parseInt(readLine.split("\\,")[j]));
        }
    }
}


