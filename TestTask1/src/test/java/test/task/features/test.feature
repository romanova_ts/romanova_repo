@mytest

Feature: task1

  Scenario: Set Mobile Phone filters

    Given open yandex.ru
    When go to Yandex section "Маркет"
    When go to Yandex-market section "Электроника"
    When go to subsection "Мобильные телефоны"
    When set price from "20000"
    And set manufacturer Apple, Samsung
    Then check that the page displays "12" elements
    When type the first element of list in the search field
    Then check that the product name matches the stored value

  Scenario: Set Headphone filters

      Given open yandex.ru
      When go to Yandex section "Маркет"
      When go to Yandex-market section "Электроника"
      When go to subsection "Наушники"
      When set price from "5000"
      And set manufacturer Beats
      Then check that the page displays "12" elements
      When type the first element of list in the search field
      Then check that the product name matches the stored value

Scenario: Sort By Price

      Given open yandex.ru
      When go to Yandex section "Маркет"
      When go to Yandex-market section "Электроника"
      When go to subsection "Мобильные телефоны"
      When sort mobile phone by price
      Then check the sort is correct
