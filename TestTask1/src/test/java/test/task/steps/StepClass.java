package test.task.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import test.task.elements.SortElements;
import test.task.hooks.Hooks;
import test.task.pages.ElectronicsPage;
import test.task.pages.HomeYandexMarketPage;
import test.task.pages.HomeYandexPage;
import test.task.elements.FilterElements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StepClass {

    HomeYandexPage homeYandexPage = new HomeYandexPage(Hooks.driver);
    HomeYandexMarketPage homeYandexMarketPage = new HomeYandexMarketPage(Hooks.driver);
    ElectronicsPage electronicsPage = new ElectronicsPage(Hooks.driver);
    FilterElements filterElements = new FilterElements(Hooks.driver);
    SortElements sortElements = new SortElements(Hooks.driver);


    @Given("open yandex.ru")
    public void openYandexRu()
    {
        Hooks.driver.get("https://yandex.ru");
    }

    @When("^go to Yandex section \"([^\"]*)\"$")
    public void goToYandexSection(String sectionName){
        homeYandexPage.clickHomeTab(sectionName);
    }

    @When("^go to Yandex-market section \"([^\"]*)\"$")
    public void goToYandexMarketSection(String sectionName){
        homeYandexMarketPage.clickNavTab(sectionName);
    }

    @When("go to subsection \"([^\"]*)\"$")
    public void goToSubSection(String subSectionName){
        electronicsPage.clickSectionName(subSectionName);
    }

    @When("set price from \"([^\"]*)\"$")
    public void setPriceFrom(String priceFrom){
        filterElements.setFilterPriceFrom(priceFrom);
    }

    @And("set manufacturer (.*)$")
    public void setPriceFrom(List<String> manfacturerList){
        filterElements.setFilterManufacturer(manfacturerList);
        Hooks.driver.navigate().refresh(); //нене придумала ничего лучше, wait.until(ExpectedConditions
    }

    @Then("check that the page displays \"([^\"]*)\" elements$")
    public void checkElementsCount(String count) {
        assertEquals(count, filterElements.getListElements().size());
    }

    @When("enter the first element of list in the search field")
    public void enterTheFirstElementInTheSearchField(){
        filterElements.searchFirstElementList();
    }

    @Then("check that the product name matches the stored value")
     public void checkStoredProductName(){
        assertEquals(filterElements.getListElements().get(0).getText(), filterElements.getSearchResult().getText());
    }

    @When("sort mobile phone by price")
    public void sortPhoneByPrice(){
        sortElements.sortByPrice();
        Hooks.driver.navigate().refresh();
    }

    @Then("check the sort is correct")
    public void checkSort(){
        List<String> expectedSort = new ArrayList();
        List<String> actualSort = new ArrayList();
        for(WebElement item : filterElements.getListOfMobilePhonePrice()){
           // item.getText().replace('₽',' ');
            expectedSort.add(item.getText());
            actualSort.add(item.getText());
        }
        Collections.sort(expectedSort);

        assertEquals(expectedSort, actualSort);
    }
}
