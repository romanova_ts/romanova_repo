package test.task.hooks;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Hooks {

    public static WebDriver driver  = null;
    public static Wait<WebDriver> wait = null;;

    @Before
    public void SetUpTest() {
        System.setProperty("webdriver.chrome.driver","libs/chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 5, 5000);
        driver.manage().timeouts().implicitlyWait(30000, TimeUnit.MILLISECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void closeBrowser(){
        driver.close();
    }
}
