package test.task.test.task.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/test/task/features",
        glue = {"test/task/steps", "test/task/hooks"},
        tags = "@mytest"
)
public class RunnerTest {

}


