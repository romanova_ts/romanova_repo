package test.task.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class FilterElements extends BaseElements {

    public FilterElements(WebDriver driver){
        super(driver);
    }

    @FindBy(id = "glpricefrom")
    private WebElement filterPriceFrom;
    @FindBy(xpath = "//fieldset[@data-autotest-id='7893318']//span")
    private List<WebElement> filterManufacturer;
    @FindBy(xpath = "//div[contains(@class,'title')]//a")
    private List<WebElement> listElements;
    @FindBy(id = "header-search")
    private WebElement searchInput;
    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnSubmit;
    @FindBy(xpath = "//h1[contains(@class,'title')]")
    private WebElement searchResult;
    @FindBy(xpath="//div[@class='price']")
    private List<WebElement> listOfMobilePhonePrice;

    public void setFilterManufacturer(List<String> manufacturerName) {

        for (WebElement menuItem : filterManufacturer) {
            for (int i = 0; i < manufacturerName.size(); i++) {
                if (menuItem.getText().trim().equals(manufacturerName.get(i))) {
                    menuItem.click();
                    break;
                }
            }
        }
    }

    public void setFilterPriceFrom(String priceFrom) {

        filterPriceFrom.sendKeys(priceFrom);
    }

    public List<WebElement> getListElements() {
        return listElements;
    }

    public void searchFirstElementList(){
        searchInput.sendKeys(listElements.get(0).getText());
        btnSubmit.click();
    }

    public WebElement getSearchResult() {
        return searchResult;
    }

    public List<WebElement> getListOfMobilePhonePrice() {
        return listOfMobilePhonePrice;
    }
}
