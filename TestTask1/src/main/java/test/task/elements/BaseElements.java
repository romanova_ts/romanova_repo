package test.task.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class BaseElements {

    protected WebDriver driver;

    protected BaseElements(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
}