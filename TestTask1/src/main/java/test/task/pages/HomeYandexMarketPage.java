package test.task.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomeYandexMarketPage extends BasePage {

    @FindBy(xpath = "//div[contains(@class,'navigation-menu')]")
    private List<WebElement> navigationTabs;

    public HomeYandexMarketPage(WebDriver driver){
        super(driver);
    }

    public void clickNavTab(String tabName){
        for(WebElement menuItem : navigationTabs){
            if(menuItem.getText().trim().equals(tabName)){
                menuItem.click();
                break;
            }
        }
    }
}
