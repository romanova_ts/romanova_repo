package test.task.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ElectronicsPage extends BasePage {

    @FindBy(xpath = "//ul[@class='_1Y6X2G3jjK']//li")
    private List<WebElement> catalogList;

    public ElectronicsPage(WebDriver driver){ super(driver);}

    public void clickSectionName(String tabName){
        for(WebElement menuItem : catalogList){
            if(menuItem.getText().trim().contains(tabName)){
                menuItem.click();
                break;
            }
        }
    }
}
