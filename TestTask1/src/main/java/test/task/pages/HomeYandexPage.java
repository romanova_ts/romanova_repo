package test.task.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomeYandexPage extends BasePage {

    @FindBy(xpath = "//div[contains(@role, 'navigation')]//a")
    private List<WebElement> homeTabs;

    public HomeYandexPage(WebDriver driver){
        super(driver);
    }

    public void clickHomeTab(String tabName){
        for(WebElement menuItem : homeTabs){
            if(menuItem.getText().trim().equals(tabName)){
                menuItem.click();
                break;
            }
        }
    }

}
